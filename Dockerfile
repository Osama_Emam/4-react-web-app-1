# Conteneur node react app
FROM node:15.10.0-alpine3.10 as build


WORKDIR /app


COPY . /app/

# Installation des dépendances 
RUN npm install -g npm@7.6.0
RUN npm install -g react-scripts@4.0.3
RUN npm install react 
# We want the production version
RUN npm run build

# Serveur nginx
FROM nginx:1.19.7-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Port nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
